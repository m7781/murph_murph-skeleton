## [Unreleased]

## 1.0.1
### Fixed
* fix Makefile environment vars (renaming)
* fix composer minimum stability

## 1.0.0
