# MURPH

Muprh is an open-source CMF built on top of Symfony that helps you to build your own CMS with several domains and languages. It comes with a fully implemented and customizable tree manager, a CRUD generator, a 2FA authentication, settings and tasks managers.

Symfony developers will love build on Murph 💪
End users will be fond of the interface and the powerful tools 💜

## [Read the documentation](https://doc.murph-project.org/)
